import { useState } from 'react';

export default function useFormInputs(initialInputValues) {
	const [inputValues, setInputValues] = useState(initialInputValues);

	const handleInputChange = (event) => {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		setInputValues({ ...inputValues, [name]: value });
	};

	return [inputValues, handleInputChange];
}
