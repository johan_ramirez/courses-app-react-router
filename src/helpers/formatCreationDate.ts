import padZeros from './padZeros';

export default function formatCreationDate(date: string) {
	const dateObj = new Date(
		parseInt(date.substring(6)),
		parseInt(date.substring(3, 5)) - 1,
		parseInt(date.substring(0, 2))
	);
	return `${padZeros(dateObj.getDate(), 2)}.${padZeros(
		dateObj.getMonth() + 1,
		2
	)}.${dateObj.getFullYear()}`;
}
