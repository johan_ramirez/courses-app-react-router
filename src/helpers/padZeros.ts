export default function padZeros(n: number, length: number): string {
	return n.toString().padStart(length, '0');
}
