function generateRandomSequence(n: number, strings: string[]) {
	let randomSequence = '';
	for (let i = 0; i < n; i++) {
		randomSequence += strings[Math.floor(Math.random() * strings.length)];
	}
	return randomSequence;
}

export default function generateAuthorId(): string {
	const chars = [
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
	];

	return (
		generateRandomSequence(8, chars) +
		'-' +
		generateRandomSequence(4, chars) +
		'-' +
		generateRandomSequence(4, chars) +
		'-' +
		generateRandomSequence(4, chars) +
		'-' +
		generateRandomSequence(12, chars)
	);
}
