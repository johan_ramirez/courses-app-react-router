import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from 'src/common/Button/Button';
import Input from 'src/common/Input/Input';
import useFormInputs from 'src/hooks/useFormInputs';

export default function Registration() {
	const [inputValues, handleInputChange] = useFormInputs({
		name: '',
		email: '',
		password: '',
	});
	const [hasErrors, setHasErrors] = useState([]);
	const navigate = useNavigate();

	const handleSubmit = (event) => {
		event.preventDefault();

		if (
			inputValues.name.length < 1 ||
			inputValues.email.length < 1 ||
			inputValues.password.length < 1
		) {
			alert('Please, fill in all form fields.');
			return;
		}

		fetch('http://localhost:4000/register', {
			method: 'POST',
			body: JSON.stringify(inputValues),
			headers: {
				'Content-Type': 'application/json',
			},
		}).then(async (response) => {
			if (response.status == 201) {
				navigate('../login');
				return;
			}

			const responseJson = await response.json();
			if (!responseJson.successful) {
				setHasErrors(responseJson.errors);
			}
		});
	};

	return (
		<div className='centered-content col gap2'>
			<h3>Register</h3>
			<form className='gap3 col align-center' onSubmit={handleSubmit}>
				<Input
					id='name'
					name='name'
					labelText='Name'
					placeholderText='Enter name'
					value={inputValues.name}
					onChange={handleInputChange}
				/>
				<Input
					id='email'
					name='email'
					labelText='Email'
					placeholderText='Enter email'
					value={inputValues.email}
					onChange={handleInputChange}
				/>
				<Input
					id='password'
					name='password'
					type='password'
					labelText='Password'
					placeholderText='Enter password'
					value={inputValues.password}
					onChange={handleInputChange}
				/>
				<Button buttonText='Register' type='submit' />
			</form>
			{hasErrors.length > 0 ? (
				<span className='text-red text-sm'>
					<ul>
						{hasErrors.map((error) => (
							<li key={error}>{error}</li>
						))}
					</ul>
				</span>
			) : (
				<></>
			)}
			<span>
				Already have an account? <Link to='/login'>Login</Link>
			</span>
		</div>
	);
}
