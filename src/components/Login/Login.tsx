import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from 'src/common/Button/Button';
import Input from 'src/common/Input/Input';
import useFormInputs from 'src/hooks/useFormInputs';

export default function Login() {
	const [inputValues, handleInputChange] = useFormInputs({
		email: '',
		password: '',
	});
	const [hasErrors, setHasErrors] = useState([]);
	const navigate = useNavigate();

	const handleSubmit = (event) => {
		event.preventDefault();

		if (inputValues.email.length < 1 || inputValues.password.length < 1) {
			alert('Please, fill in all form fields.');
			return;
		}

		fetch('http://localhost:4000/login', {
			method: 'POST',
			body: JSON.stringify(inputValues),
			headers: {
				'Content-Type': 'application/json',
			},
		}).then(async (response) => {
			const responseJson = await response.json();
			if (response.status == 201) {
				localStorage.setItem('token', responseJson.result);
				navigate('../courses');
				return;
			}

			if (!responseJson.successful) {
				setHasErrors(responseJson.errors);
			}
		});
	};

	return (
		<div className='centered-content col gap2'>
			<h3>Login</h3>
			<form className='gap3 col align-center' onSubmit={handleSubmit}>
				<Input
					id='email'
					name='email'
					labelText='Email'
					placeholderText='Enter email'
					value={inputValues.email}
					onChange={handleInputChange}
				/>
				<Input
					id='password'
					name='password'
					type='password'
					labelText='Password'
					placeholderText='Enter password'
					value={inputValues.password}
					onChange={handleInputChange}
				/>
				<Button buttonText='Login' type='submit' />
			</form>
			{hasErrors.length > 0 ? (
				<span className='text-red text-sm'>Wrong email or password</span>
			) : (
				<></>
			)}
			<span>
				Don't have an account? <Link to='/registration'>Register</Link>
			</span>
		</div>
	);
}
