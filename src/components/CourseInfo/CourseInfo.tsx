import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from 'src/common/Button/Button';
import formatCreationDate from 'src/helpers/formatCreationDate';
import getCourseDuration from 'src/helpers/getCourseDuration';
import { Author, Course } from 'src/types';
import styles from './CourseInfo.module.css';

export default function CourseInfo({
	getCourse,
	getAuthor,
}: {
	getCourse: (id: string) => Course;
	getAuthor: (id: string) => Author;
}) {
	const { courseId } = useParams();
	const course = getCourse(courseId);

	return (
		<div className='centered-content'>
			<div className={styles.courseinfo}>
				<Link to='/courses' className='mb-2'>
					<Button buttonText='Back to courses' />
				</Link>
				<span className='text-sm'>
					<b>ID: </b>
					{course.id}
				</span>
				<h1>{course.title}</h1>
				<p>{course.description}</p>
				<span>
					<b>Duration:</b> {getCourseDuration(course.duration)}
				</span>
				<span>
					<b>Author{course.authors.length > 1 ? 's' : ''}: </b>{' '}
					{course.authors.map((authorId, index) => {
						const author = getAuthor(authorId);
						return (
							author.name + (index == course.authors.length - 1 ? '' : ', ')
						);
					})}
				</span>
				<span className='text-sm mt-3'>
					<b>Creation Date</b> {formatCreationDate(course.creationDate)}
				</span>
			</div>
		</div>
	);
}
