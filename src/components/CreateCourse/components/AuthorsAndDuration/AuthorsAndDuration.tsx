import React, { useState } from 'react';
import Button from 'src/common/Button/Button';
import Input from 'src/common/Input/Input';
import AuthorList from './components/AuthorList/AuthorList';
import DurationInput from './components/DurationInput/DurationInput';

export default function AuthorsAndDuration({
	addAuthor,
	filteredAuthors,
	courseAuthors,
	removeCourseAuthor,
	addCourseAuthor,
	handleInputChange,
	durationValue,
}) {
	const [authorName, setAuthorName] = useState('');

	const handleAuthorNameChange = (event) => {
		setAuthorName(event.target.value);
	};

	const handleAddAuthor = () => {
		addAuthor(authorName);
		setAuthorName('');
	};

	return (
		<>
			<div className='row justify-between'>
				<div className='col w48 align-center gap2'>
					<span>
						<b>Add author</b>
					</span>
					<Input
						className='w100'
						id='authorName'
						name='authorName'
						labelText='Author name'
						placeholderText='Enter author name'
						value={authorName}
						onChange={handleAuthorNameChange}
					/>
					<Button buttonText='Create author' onClick={handleAddAuthor} />
				</div>
				<div className='col w48 align-center gap2'>
					<span>
						<b>Authors</b>
					</span>
					<AuthorList authors={filteredAuthors} handleClick={addCourseAuthor} />
				</div>
			</div>
			<div className='row justify-between'>
				<DurationInput value={durationValue} onChange={handleInputChange} />
				<div className='col w48 align-center gap2'>
					<span>
						<b>Course authors</b>
					</span>
					<AuthorList
						authors={courseAuthors}
						courseAuthors
						handleClick={removeCourseAuthor}
					/>
				</div>
			</div>
		</>
	);
}
