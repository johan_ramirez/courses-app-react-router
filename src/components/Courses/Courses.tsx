import React, { useMemo, useState } from 'react';
import styles from './Courses.module.css';
import Button from 'src/common/Button/Button';
import { Author, Course } from 'src/types';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import { Link } from 'react-router-dom';

export default function Courses({
	courses,
	authors,
}: {
	courses: Course[];
	authors: Author[];
}) {
	const coursesWithAuthorNames = useMemo(
		() =>
			courses.map((course) => ({
				...course,
				authors: course.authors.map(
					(authorId) => authors.find((author) => authorId == author.id).name
				),
			})),
		[courses]
	);
	const [searchedCourses, setSearchedCourses] = useState(
		coursesWithAuthorNames
	);

	const search = (searchTerm: string) => {
		setSearchedCourses(
			coursesWithAuthorNames.filter(
				(course) =>
					course.id
						.toLocaleLowerCase()
						.includes(searchTerm.toLocaleLowerCase()) ||
					course.title
						.toLocaleLowerCase()
						.includes(searchTerm.toLocaleLowerCase())
			)
		);
	};

	return (
		<div className={styles.courses}>
			<div className='row'>
				<SearchBar search={search} />
				<span className='spacer'></span>
				<Link to='/courses/add'>
					<Button buttonText='Add new course' />
				</Link>
			</div>
			<div className={styles.coursecards}>
				{searchedCourses.map((course) => (
					<CourseCard key={course.id} {...course} />
				))}
			</div>
		</div>
	);
}
