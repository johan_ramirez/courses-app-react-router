import React from 'react';
import styles from './CourseCard.module.css';
import getCourseDuration from 'src/helpers/getCourseDuration';
import formatCreationDate from 'src/helpers/formatCreationDate';
import Button from 'src/common/Button/Button';
import { Course } from 'src/types';
import { Link } from 'react-router-dom';

export default function CourseCard({
	id,
	title,
	description,
	creationDate,
	duration,
	authors,
}: Course) {
	return (
		<div className={styles.coursecard + ' row justify-between'}>
			<div className='col w60'>
				<h3>{title}</h3>
				<span>{description}</span>
			</div>
			<div className='col w30 gap2'>
				<span className={styles.authors}>
					<b>Authors:</b>{' '}
					{authors.map(
						(v, index) => v + (index == authors.length - 1 ? '' : ', ')
					)}
				</span>
				<span>
					<b>Duration:</b> {getCourseDuration(duration)}
				</span>
				<span>
					<b>Created:</b> {formatCreationDate(creationDate)}
				</span>
				<span className='spacer' />
				<div className='row justify-center'>
					<Link to={`/courses/${id}`}>
						<Button buttonText='Show Course' />
					</Link>
				</div>
			</div>
		</div>
	);
}
