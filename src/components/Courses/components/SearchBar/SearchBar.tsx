import React, { useState } from 'react';
import Button from 'src/common/Button/Button';
import styles from './SearchBar.module.css';

export default function SearchBar({ search }) {
	const [searchValue, setSearchValue] = useState('');

	const handleSearchChange = (event) => {
		const value = event.target.value;
		if (value == '') search('');
		setSearchValue(value);
	};

	const handleInputEnter = (event) => {
		if (event.keyCode == 13) {
			search(searchValue);
		}
	};

	return (
		<div className={styles.searchbar}>
			<input
				className={styles.input}
				value={searchValue}
				onChange={handleSearchChange}
				onKeyDown={handleInputEnter}
			/>
			<Button buttonText='Search' onClick={() => search(searchValue)} />
		</div>
	);
}
