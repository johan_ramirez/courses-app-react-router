import React from 'react';
import logo from '../../../../assets/logo.svg';
import './Logo.css';

const Logo = () => <img className='logo' src={logo} />;

export default Logo;
