import React, { useEffect, useState } from 'react';
import Logo from './components/Logo/Logo';
import Button from 'src/common/Button/Button';
import styles from './Header.module.css';
import { Link, useLocation, useNavigate } from 'react-router-dom';

function Header() {
	const [showUser, setShowUser] = useState(true);
	const [user, setUser] = useState(null);
	const [isAuthenticated, setIsAuthenticated] = useState(false);

	const location = useLocation();

	useEffect(() => {
		if (location.pathname == '/login' || location.pathname == '/registration')
			setShowUser(false);
		else setShowUser(true);
		const isAuthenticated = localStorage.getItem('token');
		setIsAuthenticated(!!isAuthenticated);
	}, [location]);

	useEffect(() => {
		if (isAuthenticated) {
			fetch('http://localhost:4000/users/me', {
				headers: {
					Authorization: localStorage.getItem('token'),
				},
			}).then(async (response) => {
				const responseJson = await response.json();
				setUser(responseJson.result.name);
			});
		}
	}, [isAuthenticated]);

	const navigate = useNavigate();

	const handleLogout = () => {
		localStorage.removeItem('token');
		navigate('/login');
	};

	return (
		<header className={styles.header}>
			<Logo />
			<span className='spacer' />
			{showUser ? (
				isAuthenticated ? (
					<>
						<span>{user}</span>
						<Button buttonText='Logout' onClick={handleLogout} />
					</>
				) : (
					<>
						<Link to='/login'>
							<Button buttonText='Login' />
						</Link>
						<Link to='/registration'>Register</Link>
					</>
				)
			) : (
				''
			)}
		</header>
	);
}

export default Header;
