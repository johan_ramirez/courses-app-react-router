import React, { useEffect, useState } from 'react';
import Header from './components/Header/Header';
import { mockedAuthorsList, mockedCoursesList } from './constants';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import generateAuthorId from './helpers/generateAuthorId';
import { Course } from './types';
import {
	BrowserRouter,
	Navigate,
	Route,
	Routes,
	useLocation,
} from 'react-router-dom';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

enum PageStates {
	courses,
	newCourse,
}

function App() {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const addAuthor = (name: string) => {
		if (name.trim().length < 1) {
			alert('Author name should not be empty');
			return;
		}

		setAuthors([
			...authors,
			{
				id: generateAuthorId(),
				name: name,
			},
		]);
	};

	const addCourse = (course: Course) => {
		setCourses([...courses, course]);
	};

	const getCourse = (id: string) => {
		return courses.find((course) => course.id == id);
	};

	const getAuthor = (id: string) => {
		return authors.find((author) => author.id == id);
	};

	const isAuthenticated = localStorage.getItem('token');

	return (
		<>
			<BrowserRouter>
				<Header />
				<Routes>
					<Route
						path='/courses'
						element={<Courses courses={courses} authors={authors} />}
					/>
					<Route
						path='/courses/add'
						element={
							<CreateCourse
								authors={authors}
								addAuthor={addAuthor}
								addCourse={addCourse}
							/>
						}
					/>
					<Route
						path='/courses/:courseId'
						element={<CourseInfo getCourse={getCourse} getAuthor={getAuthor} />}
					/>
					<Route path='/registration' element={<Registration />} />
					<Route path='/login' element={<Login />} />
					<Route
						path='*'
						element={<Navigate to={isAuthenticated ? '/courses' : '/login'} />}
					/>
				</Routes>
			</BrowserRouter>
		</>
	);
}

export default App;
