import React from 'react';
import styles from './Button.module.css';

interface ButtonProps {
	buttonText?: string;
	onClick?: React.MouseEventHandler;
	type?: 'button' | 'submit' | 'reset';
}

const Button = ({ buttonText, onClick, type = 'button' }: ButtonProps) => (
	<button className={styles.btn} onClick={onClick} type={type}>
		{buttonText}
	</button>
);

export default Button;
