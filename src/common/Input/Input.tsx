import React from 'react';
import styles from './Input.module.css';

interface InputProps {
	id: string;
	name?: string;
	labelText?: string;
	placeholderText?: string;
	onChange?: React.ChangeEventHandler;
	value?: string;
	type?: string;
	className?: string;
}

export default function Input({
	id,
	name,
	labelText,
	placeholderText,
	onChange,
	value,
	type = 'text',
	className,
}: InputProps) {
	return (
		<span className={styles.inputWrapper + ' col ' + className}>
			<label className={styles.label} htmlFor={id}>
				{labelText}
			</label>
			<input
				name={name}
				className={styles.input}
				type={type}
				value={value}
				onChange={onChange}
				id={id}
				placeholder={placeholderText}
			/>
		</span>
	);
}
